D-Genies Viewer
====================


About
-------------------------------------------
D-Genies viewer is a simple viewer based on [D-Genies](http://dgenies.toulouse.inra.fr). It's a HTML web page with a javascript application inside which display the dotplot. As it's only HTML and Javascript (no backend server), all server functions, including sort and noise are removed from this version. Fortunately, you can apply them command line, when you parse the map file (see below).


How to use
----------------------

We suppose you have a target (and eventually a query) fasta file (we will name them `target.fa` and `query.fa`, respectively). These files can be gzipped.

We suppose you have already launch [minimap2](https://github.com/lh3/minimap2) on this data. So you have a PAF file. We will name it `map.paf`.

### 1. Prepare data

Go into the `parser` folder, and run the `parser.py` script, like this:

```
./parser.py -p /path/to/map.paf -q /path/to/query.fa -t /path/to/targer.fa
```

#### Sort 

You can add the `-s` option to sort the contigs (query) according to the target order.

#### Noise

You can add the `-n` option to remove the noise.

#### Target and Query names

You can specify the target and query names by the `-u` and `-r` options, respectively. If not specified, names are name of file, without extension.

#### Note

The `-o` option is used to specify the output json file path. Except if you know what you are doing, never use it and keep default value.

### 2. Show result

You can now access to the `index.html` file throw a web server (like Apache) to show the results.

Important: you need to keep all files (except the parser folder, enventually) in the same folder as the index.html file!

© INRA 2017