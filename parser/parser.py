#!/usr/bin/env python3

import os
import shutil
import argparse
from lib.fasta import Fasta
from lib.functions import Functions
from lib.paf import Paf
from sort_paf import Sorter

app_folder = os.path.dirname(os.path.realpath(__file__))


def _parse_args():
    parser = argparse.ArgumentParser(description="Build index for query and target, which defines contigs and "
                                                 "chromosomes positions")
    parser.add_argument('-p', '--paf', type=str, required=True, help="Input PAF file from minimap2")
    parser.add_argument('-q', '--query', type=str, required=False, help="Query fasta file compared with minimap2")
    parser.add_argument('-t', '--target', type=str, required=True, help="Target fasta file compared with minimap2")
    parser.add_argument('-s', '--sort', action='store_const', const=True, default=False,
                        help='Sort contigs accorting to target')
    parser.add_argument('-n', '--no-noise', action='store_const', const=True, default=False,
                        help='Remove noise')
    parser.add_argument('-r', '--query-name', type=str, required=False, help="Query name")
    parser.add_argument('-u', '--target-name', type=str, required=False, help="Target name")
    parser.add_argument('-o', '--out', type=str, required=False, help="Json output file")

    return parser.parse_args()


def parse(paf, target, query=None, out=None, query_name=None, target_name=None, sort_c=False, noise=True):
    # Some checks:
    if not os.path.exists(target):
        raise Exception("Fasta file %s does not exists!" % target)
    if args.query is not None and not os.path.exists(query):
        raise Exception("Fasta file %s does not exists!" % args)
    if not os.path.exists(paf):
        raise Exception("Paf file %s does not exists!" % target)

    # Define missing variables (if any):
    if out is None:
        out = os.path.join(os.path.dirname(app_folder), "map.json")

    if query_name is None and query is not None:
        query_name = os.path.basename(query)
        if query_name.endswith(".gz"):
            query_name = query_name[:-3]
        query_name = query_name.rsplit(".", 1)[0]

    if target_name is None:
        target_name = os.path.basename(target)
        if target_name.endswith(".gz"):
            target_name = target_name[:-3]
        target_name = target_name.rsplit(".", 1)[0]

    # Temp dir:
    tmp_dir = os.path.join(app_folder, "tmp")
    if os.path.exists(tmp_dir):
        if os.path.isfile(tmp_dir):
            os.remove(tmp_dir)
        else:
            shutil.rmtree(tmp_dir)
    os.makedirs(tmp_dir)

    # Index query and target:
    print("Indexing...")
    target_idx = os.path.join(tmp_dir, "target.idx")
    query_idx = os.path.join(tmp_dir, "query.idx")
    target_f = Fasta(target_name, target, "local")
    Functions.index_file(target_f, target_idx)
    if query is not None:
        query_f = Fasta(query_name, query, "local")
        Functions.index_file(query_f, query_idx)
    else:
        shutil.copy(target_idx, query_idx)

    # Sort paf:
    print("Prepare PAF file...")
    o_paf = os.path.join(tmp_dir, "map.paf")
    sorter = Sorter(input_f=paf, output_f=o_paf)
    sorter.sort()

    # Parse Paf file and save to Json:
    print("Build Json...")
    paf_c = Paf(paf=o_paf,
                idx_q=query_idx,
                idx_t=target_idx,
                auto_parse=not sort_c)
    if sort_c:
        print("Sort contigs...")
        paf_c.sorted = False
        paf_c.sort()  # Parse is done just after sort in the function
    if not noise:
        print("Remove noise...")
        paf_c.parse_paf(noise=False)
    if paf_c.parsed:
        paf_c.save_json(out=out)
    else:
        print("ERROR: unable to parse file!")
        exit(1)

    # Cleaning:
    print("Cleaning...")
    shutil.rmtree(tmp_dir)

    print("Done!")


if __name__ == '__main__':
    args = _parse_args()
    parse(paf=args.paf,
          query=args.query,
          target=args.target,
          out=args.out,
          query_name=args.query_name,
          target_name=args.target_name,
          sort_c=args.sort,
          noise=not args.no_noise)
