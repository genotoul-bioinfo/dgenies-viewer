#!/usr/bin/env python3

"""
Build index for query and target, which defines contigs and chromosomes positions
"""

__NAME__ = "Build_dgenies_index"
__VERSION__ = 0.1

import os
import argparse
from lib.functions import Functions
from lib.fasta import Fasta


def _parse_args():
    parser = argparse.ArgumentParser(description="Build index for query and target, which defines contigs and "
                                                 "chromosomes positions")
    parser.add_argument('-q', '--query', type=str, required=True, help="Query fasta file compared with minimap")
    parser.add_argument('-t', '--target', type=str, required=True, help="Target fasta file compared with minimap")
    parser.add_argument('-o', '--out-dir', type=str, required=True, help="Output directory")
    parser.add_argument('-r', '--query-name', type=str, required=False, help="Query name")
    parser.add_argument('-y', '--target-name', type=str, required=False, help="Target name")

    return parser.parse_args()


def init(output_d, query, target, query_name=None, target_name=None):
    query_f = Fasta(query_name, query, "local")
    target_f = Fasta(target_name, target, "local")
    Functions.index_file(query_f, os.path.join(output_d, "query.idx"))
    Functions.index_file(target_f, os.path.join(output_d, "target.idx"))


if __name__ == '__main__':
    args = _parse_args()
    if not os.path.exists(args.query):
        raise Exception("Fasta file %s does not exists!" % args.query)
    if not os.path.exists(args.target):
        raise Exception("Fasta file %s does not exists!" % args.target)
    init(args.out_dir, args.query, args.target, args.query_name, args.target_name)
